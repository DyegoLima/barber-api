import express, { NextFunction, Request, Response } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import versions from './api';
import path from 'path';
import ConstantsStatus from './constants/http-status'



const app = express()


app.use(cors())

app.use(bodyParser.json({
  limit: '100mb'
}))
app.use(bodyParser.urlencoded({
  limit: '100mb',
  extended: false
}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

function startRoutes() {
  versions.forEach(version => {
    version.routes.forEach(item => {
      console.log(`${version.name}/${item.name}`)
      app.use(`/${version.name}/${item.name}`, item.route)
    })
  })
}

startRoutes()

app.use((req: Request, res: Response) => {
  res.status(ConstantsStatus.bad_request)
      .json({
          message: 'Not Found'
      })
})

app.use((error: any, _: any, res: Response, next: NextFunction) => {
  console.error('======================= ERROR ======================= \n', error)
  error.message = error.message.replace('Validation error: ', '')
  res.status(error.status || ConstantsStatus.bad_request).json({
      message: error.message
  })
  next()
})

export default app