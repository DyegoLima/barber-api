import i18n  from 'i18n'

i18n.configure({
    locales: ['en', 'pt'],
    directory: `${__dirname}/locales`,
    defaultLocale: 'pt',
    register: global
})

export default i18n
