import sqlServer from '../../../modules/sqlServer'

const getCostumerByPassword = (email: string, password: string) => sqlServer.costumer.findOne({ where: { password, email } })

export default {
  getCostumerByPassword
}
