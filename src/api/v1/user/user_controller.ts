import { NextFunction, Request, Response } from 'express'
import UserBusiness from './user_business'



const UserController = {

  loginCostumer: async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = await UserBusiness.loginCostumer(req.body)
        res.json(user)
    } catch (err) {
        next(err)
    }
}
}

export default UserController
