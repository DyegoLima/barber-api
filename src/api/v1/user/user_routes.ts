import express from 'express'
import userController from './user_controller'
import Token from '../../../utils/token'

const router = express.Router()

router.post('/auth/costumer', (req, res, next) => userController.loginCostumer(req, res, next))
router.post('/auth/encrypt', async (req, res) => res.json({ token: await Token.generateToken(req.body) }))
router.get('/auth/decrypt', async (req, res) => res.json(await Token.validToken(req.query.token)))

export default router
