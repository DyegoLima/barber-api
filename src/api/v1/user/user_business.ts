import userDao from './user_dao'
import Token from '../../../utils/token'


const UserBusiness = {

  loginCostumer: async ({ username, password }: any) => {
    let user = await userDao.getCostumerByPassword(username, password)
    if (user == null) {
      throw new Error('Usuário ou senha inválidos')
    }

    user = user.toJSON()
    delete user.password
   

    user.token = await Token.generateToken(user)
    return user
  }
}
export default UserBusiness

