import costumerDao from './costumer_dao'

const CostumerBusiness = {

    getAll: async () => await costumerDao.getAll(),
    get: async (id: string) => await costumerDao.get(id),
    remove: async (id: string) => await costumerDao.remove(id),
    update: async (id: string, data: object) => await costumerDao.update(id, data),
    create: async (value: object) => await costumerDao.create(value)
}
export default CostumerBusiness

