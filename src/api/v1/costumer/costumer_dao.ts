import sqlServer from '../../../modules/sqlServer'

const getAll = () => sqlServer.costumer.findAll()
const get = (id: string) => sqlServer.costumer.findByPk(id)
const remove = (id: string) => sqlServer.costumer.destroy({ where: { id } })
const update = (id: string, data: object) => sqlServer.costumer.update(data, { where: { id } })
const create = (value: object) => sqlServer.costumer.create(value)

export default {
  getAll,
  create,
  get,
  remove,
  update
}
