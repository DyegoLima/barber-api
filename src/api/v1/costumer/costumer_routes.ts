import express from 'express'
import costumerController from './costumer_controller'

const router = express.Router()

router.get('/', (req, res, next) => costumerController.getAll(req, res, next))
router.get('/:id', (req, res, next) => costumerController.get(req, res, next))
router.delete('/:id', (req, res, next) => costumerController.remove(req, res, next))
router.put('/:id', (req, res, next) => costumerController.update(req, res, next))
router.post('/', (req, res, next) => costumerController.create(req, res, next))

export default router
