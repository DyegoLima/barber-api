import { NextFunction, Request, Response } from 'express'
import CostumerBusiness from './costumer_business'



const CostumerController = {

  getAll: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const costumers = await CostumerBusiness.getAll()
      res.json(costumers)
    } catch (err) {
      next(err)
    }
  },


  get: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const costumer = await CostumerBusiness.get(req.params.id)
      res.json(costumer)
    } catch (err) {
      next(err)
    }
  },

  remove: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const costumer = await CostumerBusiness.remove(req.params.id)
      res.json(costumer)
    } catch (err) {
      next(err)
    }
  },

  update: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const costumer = await CostumerBusiness.update(req.params.id, req.body)
      res.json(costumer)
    } catch (err) {
      next(err)
    }
  },

  create: async (req: any, res: any, next: any) => {
    try {
      const costumer = await CostumerBusiness.create(req.body)
      res.json(costumer)
    } catch (err) {
      console.log(err.response)
      next(err)
    }
  }
}

export default CostumerController
