import CostumerRoutes from './costumer/costumer_routes'
import UserRoutes from './user/user_routes'

export default [
    {
        route: CostumerRoutes, name: 'costumer'
    },
    {
        route: UserRoutes, name: 'user'
    }
]
