import { Sequelize } from 'sequelize'
import models from './models'

const sequelize = new Sequelize(
    process.env.MYSQL_DATABASE!,
    process.env.MYSQL_USER!,
    process.env.MYSQL_PASSWORD,
    {
        host: process.env.MYSQL_HOST,
        dialect: 'mssql',
        dialectOptions: {
            options: {
                validateBulkLoadParameters: true
            },
            ssl: {
                require: false,
                rejectUnauthorized: false
            },
            requestTimeout: 30000,
            charset: 'utf8mb4',
            useUTC: false,
            dateStrings: true,
            typeCast(field: any, next: any) {
                if (field.type === 'DATETIME') {
                    return field.string()
                }
                return next()
            }
        }
        // timezone: '-03:00',
    },
)
const db: any = {}

models.forEach((item: any) => {
    const model = item(sequelize)
    db[model.name] = model
})

Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db)
    }
})

db.sequelize = sequelize


export default db
