import { DataTypes } from 'sequelize'

export default (sequelize: any) => {
    const Costumer = sequelize.define(
        'costumer', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: {
                    msg: 'O campo Nome não pode ser vazio'
                }
            }
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: {
                    msg: 'O campo Sobrenome não pode ser vazio'
                }
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: {
                    msg: 'O campo Senha não pode ser vazio'
                }
            }
        },
        phone: {
            type: DataTypes.STRING,
            validate: {
                len: {
                    args: [9, 10],
                    msg: 'O campo Telefone deve conter pelo menos 8 dígitos'
                }
            }
        },
        phoneDdd: {
            type: DataTypes.STRING,
            validate: {
                isNumeric: {
                    msg: 'O campo Telefone deve conter pelo menos 3 dígitos'
                },
                len: {
                    args: [3, 3],
                    msg: 'O campo Telefone deve conter pelo menos 8 dígitos'
                }
            }
        },
        reciveEmail: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: true
        },
        email: {
            type: DataTypes.STRING,
            unique: {
                name: 'email',
                msg: 'O E-mail já foi utilizado'
            },
            allowNull: false,
            validate: {
                notEmpty: {
                    msg: 'O campo E-mail não pode ser vazio'
                },
                isEmail: {
                    msg: 'O campo E-mail é inválido'
                }
            }
        }
    },
        {
            freezeTableName: true
        },
    )

    return Costumer
}
