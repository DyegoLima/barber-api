const validCPF = (incomingCPF: string) => {
  const strCPF = incomingCPF.replace(/[^\d]+/g, '')

  let Soma
  let Resto
  Soma = 0
  if (strCPF === '00000000000') return false

  for (let i = 1; i <= 9; i += 1) Soma += parseInt(strCPF.substring(i - 1, i), 10) * (11 - i)
  Resto = (Soma * 10) % 11

  if ((Resto === 10) || (Resto === 11)) Resto = 0
  if (Resto !== parseInt(strCPF.substring(9, 10), 10)) return false

  Soma = 0
  for (let i = 1; i <= 10; i += 1) Soma += parseInt(strCPF.substring(i - 1, i), 10) * (12 - i)
  Resto = (Soma * 10) % 11

  if ((Resto === 10) || (Resto === 11)) Resto = 0
  if (Resto !== parseInt(strCPF.substring(10, 11), 10)) return false
  return true
}

const validarCNPJ = (incomingCNPJ: string) => {
  const cnpj = incomingCNPJ.replace(/[^\d]+/g, '')

  if (cnpj === '') return false

  if (cnpj.length !== 14) { return false }

  // Elimina CNPJs invalidos conhecidos
  if (cnpj === '00000000000000' ||
    cnpj === '11111111111111' ||
    cnpj === '22222222222222' ||
    cnpj === '33333333333333' ||
    cnpj === '44444444444444' ||
    cnpj === '55555555555555' ||
    cnpj === '66666666666666' ||
    cnpj === '77777777777777' ||
    cnpj === '88888888888888' ||
    cnpj === '99999999999999') { return false }

  // Valida DVs
  let tamanho = cnpj.length - 2
  let numeros = cnpj.substring(0, tamanho)
  const digitos = cnpj.substring(tamanho)
  let soma = 0
  let pos = tamanho - 7
  for (let i = tamanho; i >= 1; i -= 1) {
      soma += numeros.charAt(tamanho - i) * pos
      pos -= 1
      if (pos < 2) { pos = 9 }
  }
  let resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11)
  if (resultado !== parseInt(digitos.charAt(0), 10)) { return false }

  tamanho += 1
  numeros = cnpj.substring(0, tamanho)
  soma = 0
  pos = tamanho - 7
  for (let i = tamanho; i >= 1; i -= 1) {
      soma += numeros.charAt(tamanho - i) * pos
      pos -= 1
      if (pos < 2) { pos = 9 }
  }
  resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11)
  if (resultado !== parseInt(digitos.charAt(1), 10)) { return false }

  return true
}

const getNameTableOfContent = (type: string) => {
  let table = ''

  if (!type) return table

  switch (type) {
  case 'EVENT':
      table = 'events'
      break
  case 'COMMUNICATION':
      table = 'communications'
      break
  case 'NEWS':
      table = 'news'
      break
  case 'VOTE':
      table = 'votes'
      break
  case 'DEMAND':
      table = 'demands'
      break
  default:
      break
  }

  return table
}

const getNameField = (table: string) => {
  let nameRelationship = ''

  if (!table) return nameRelationship

  if (table === 'events') {
      nameRelationship = 'eventId'
  }

  if (table === 'communications') {
      nameRelationship = 'communicationId'
  }

  if (table === 'news') {
      nameRelationship = 'newsId'
  }

  if (table === 'votes') {
      nameRelationship = 'voteId'
  }

  if (table === 'demands') {
      nameRelationship = 'demandId'
  }

  return nameRelationship
}

const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

export default {
  validCPF,
  validarCNPJ,
  getNameTableOfContent,
  getNameField,
  capitalize
}

