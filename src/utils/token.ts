import jwt from 'jsonwebtoken'

class Token {
    static generateToken(data: Body) {
        return new Promise((resolve, reject) => {
            jwt.sign(data, process.env.JWT_SECRET_AUTHENTICATION!, (err: any, token: any) => (err ? reject(err) : resolve(token)))
        })
    }

    static validToken(token: any) {
        return new Promise((resolve, reject) => {
            if (token) {
                jwt.verify(token, process.env.JWT_SECRET_AUTHENTICATION!, (err: any, decoded: any) => (err ? reject(err) : resolve(decoded)))
            }
            const error = {
                message: 'invalid token'
            }
            return reject(error)
        })
    }
}

export default Token
