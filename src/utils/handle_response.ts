import { get, isEmpty, map, merge } from 'lodash'
import caseConverter from 'case-converter'
import I18n from '../i18n'

class HandleResponse {
    static forbidden(message: string) {
        const errorMessage = message || 'Forbidden'
        return HandleResponse._errorReponse(403, errorMessage)
    }

    static notFound(message: string) {
        const errorMessage = message || 'Resource not found'
        return HandleResponse._errorReponse(404, errorMessage)
    }

    static unauthorized(message: string) {
        const errorMessage = message || 'Missing token or invalid token'
        return HandleResponse._errorReponse(401, errorMessage)
    }

    static unprocessableEntity(message: string) {
        const errorMessage = message || 'Invalid credentials or missing parameters'
        return HandleResponse._errorReponse(422, errorMessage)
    }

    static handleError(error: Error) {
        let message = error.message || 'Undefined error'

        const errorList = get(error, 'errors')

        if (isEmpty(errorList) === false) {
            const errorMessageList = map(errorList, (errorItem: { message: any }) => errorItem.message || 'Undefined error')

            message = errorMessageList.toString()
        }

        const nameStatusMap = {
            SequelizeUniqueConstraintError: 409
        }

        const status = nameStatusMap[error.name]

        return HandleResponse._errorReponse(status, message)
    }

    static _errorReponse(status: number, message: string) {
        const error = new Error()
        error.status = status || 400
        error.message = message || 'Bad request'
        return error
    }

    static success(data: any, message = 'Successful', additionalData = {}) {
        const defaultDataResponse = {
            data: caseConverter.toCamelCase(data),
            message: I18n.__(message)
        }

        const dataResponse = merge({}, defaultDataResponse, additionalData)

        return dataResponse
    }
}

export default HandleResponse
