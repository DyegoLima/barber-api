import app from '../app'
import config from '../config/aplication.json'
import { AddressInfo } from 'net';
import sqlServer from '../modules/sqlServer'
import dotenv from 'dotenv'

dotenv.config()
const port = process.env.PORT || config.application.port;

(async () => {
  await sqlServer.sequelize.sync()
  const server = app.listen(port, () => {
    console.log('------ RUN APPLICATION ------')
    console.log('PORT: ', (<AddressInfo>server.address()).port)
  })
})()

